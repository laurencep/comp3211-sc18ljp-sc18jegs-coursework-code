# COMP3211 Distributed Systems - sc18ljp & sc18jegs - Coursework Code

This repository is a compilation of code written by sc18jegs and sc18ljp for the Distributed Systems Coursework:

- `./Web Service 1` contains a copy of the git repository containing code written by sc18jegs for the first web service.
- `./Web Service 2` contains a copy of the git repository containing code written by sc18ljp for the second web service.
- `./Client` contains a copy of the git repository containing code written by sc18jegs and sc18ljp for the integrated client.
- `measure_response_time.py` is a Python 3 script written by sc18jegs and sc18ljp used to measure the response time of an API endpoint.
