import express from "express";
import admin from "firebase-admin";

// Create express app.
const app = express();

// Get Firebase credentials from environment variables.
const serviceAccount: admin.ServiceAccount = {
  projectId: process.env.FIREBASE_PROJECT_ID,
  privateKey: process.env.FIREBASE_PRIVATE_KEY!.replace(/\\n/g, "\n"),
  clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
};

// Initialise Firebase.
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});
const db = admin.firestore();

// Set HTTP headers.
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Endpoint to return information about a room, given a room code.
app.get("/rooms/:code", async (req, res) => {
  // Query database for room code.
  let result = await db
    .collectionGroup("rooms")
    .where("code", "==", req.params.code)
    .get();

  // Return JSON of document if found, 404 if not.
  if (result.empty) {
    res.status(404).send("Room not found");
  } else {
    res.json(result.docs[0].data());
  }
});

// Endpoint to return information about a building, given a room code.
app.get("/rooms/:code/building", async (req, res) => {
  // Query database for room code.
  let roomQuery = await db
    .collectionGroup("rooms")
    .where("code", "==", req.params.code)
    .get();

  // If no room found send 404.
  if (roomQuery.empty) {
    res.status(404).send("Room not found");
    // If room found, find building.
  } else {
    // Get the document representing the building the room is in.
    let buildingQuery = await roomQuery.docs[0].ref.parent.parent?.get();
    let buildingData = buildingQuery?.data();

    // Return JSON document.
    res.json(buildingData);
  }
});

// Export Express app to be used by index.ts and test.js.
export default app;
