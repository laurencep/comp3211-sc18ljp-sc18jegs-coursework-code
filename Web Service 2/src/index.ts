import app from "./app";

// Set port from environment variable or fall back to default.
const port = process.env.PORT || 3000;

// Start HTTP server listening on given port.
app.listen(port, () => {
  return console.log(`server is listening on port ${port} `);
});
