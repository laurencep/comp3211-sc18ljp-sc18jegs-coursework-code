# University Facilities Directory API

This TypeScript NodeJS API provides access to information about a fictional university's room and buildings.

## Endpoints

- `/rooms/<room code>` - Gets information about a room with room code `<room code>`.
- `/rooms/<room code>/building` - Gets information about a building containing the room with room code `<room code>`.

## Project Structure

- `./src` contains the API source code:
  - `app.ts` contains the main API source code.
  - `index.ts` acts as a wrapper to create an instance of the API and serve it on a given port.
- `./test` contains the Mocha code used to test the API.
- `startup.sh` contains the command Azure runs in order to start up the API once deployed.
- `tsconfig.json` is the TypeScript configuration file.
- The built files are placed in `./build`

## Local Development

In order to run or test the API locally the following prerequisite steps must be followed:

1. Install dependencies and dev dependencies listed in `package.json`.
2. Create a `.env` file and specify the following environment variables:

   - `PORT` - The local port the API should listen on.
   - `FIREBASE_PROJECT_ID` - The project ID from the Firebase service account.
   - `FIREBASE_PRIVATE_KEY` - The private key from the Firebase service account.
   - `FIREBASE_CLIENT_EMAIL` - The client email from the Firebase service account.

### Running the API

To run this API locally use command `npm run dev` to start the server.

### Testing

Mocha unit tests have been written to test the enpoints. These can be run by using the command `npm test`.
