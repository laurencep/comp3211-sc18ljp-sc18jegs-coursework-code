const app = require("../build/app").default;
const request = require("superagent");
const _ = require("lodash");

// Before running the tests, run the API locally.
let server;
before(() => {
  server = app.listen(3000);
  console.log("listening");
});

// Define main test set.
describe("API", function () {
  // Set timeout to 10 seconds.
  this.timeout(10000);

  // Define test set for getting information about a room.
  describe("roomInfo", function () {
    // Test that valid info is returned when given a valid room code.
    it("should return the correct room info when given a valid room", function (done) {
      let expected = {
        capacity: 50,
        code: "WB2.05",
        type: "lecture theatre",
      };

      // Call API.
      request
        .get("http://localhost:3000/rooms/WB2.05")
        .then((response) => {
          // If response as expected, test passes.
          if (_.isEqual(expected, response.body)) {
            done();
            // if response different, test fails.
          } else {
            done(
              new Error(
                `Expected ${JSON.stringify(expected)}, got ${JSON.stringify(
                  response.body
                )}`
              )
            );
          }
        })
        // If error, test fails.
        .catch((e) => {
          if (e.response.statusCode == 404) {
            done(new Error("API couldn't find room."));
          } else {
            done(new Error("Other API error."));
          }
        });
    });

    // Test that 404 is returned if room not found.
    it("should return a 404 error when a room is not found", function (done) {
      let expected = 404;

      // Call API.
      request
        .get("http://localhost:3000/rooms/imaginary.room")
        .then((response) => {
          // If valid response returned, test fails.
          done(new Error("API found a room that shouldn't exist"));
        })
        .catch((e) => {
          // If returned status code is 404, test passes.
          if (_.isEqual(expected, e.response.statusCode)) {
            done();
            // If any other error, test fails.
          } else {
            done(
              new Error(
                `Expected ${JSON.stringify(expected)}, got ${JSON.stringify(
                  e.response.statusCode
                )}`
              )
            );
          }
        });
    });
  });

  // Define test set for getting information about a building.
  describe("buildingInfo", function () {
    // Test that valid information is returned when a valid room code passed.
    it("should return building information when given a valid room", function (done) {
      let expected = {
        coordinates: {
          _latitude: 53.80908949510756,
          _longitude: -1.5539358858116676,
        },
        department: "computing",
        name: "Watkins Bufford",
      };

      // Call API.
      request
        .get("http://localhost:3000/rooms/WB2.05/building")
        .then((response) => {
          // If response as expected, test passes.
          if (_.isEqual(expected, response.body)) {
            done();
            // If response different, test fails.
          } else {
            done(
              new Error(
                `Expected ${JSON.stringify(expected)}, got ${JSON.stringify(
                  response.body
                )}`
              )
            );
          }
        })
        // If error, test fails.
        .catch((e) => {
          if (e.response.statusCode == 404) {
            done(new Error("API couldn't find room."));
          } else {
            done(new Error("Other API error."));
          }
        });
    });

    // Test that a 404 is returned when given an invalid room.
    it("should return a 404 error when a room is not found", function (done) {
      let expected = 404;

      // Call API.
      request
        .get("http://localhost:3000/rooms/imaginary.room/building")
        .then((response) => {
          // If valid response returned, test fails.
          done(new Error("API found a room that shouldn't exist"));
        })
        .catch((e) => {
          // If returned status code is 404, test passes.
          if (_.isEqual(expected, e.response.statusCode)) {
            done();
            // If any other error, test fails.
          } else {
            done(
              new Error(
                `Expected ${JSON.stringify(expected)}, got ${JSON.stringify(
                  e.response.statusCode
                )}`
              )
            );
          }
        });
    });
  });
});

// After running tests, close local instance down.
after(() => {
  server.close();
});
