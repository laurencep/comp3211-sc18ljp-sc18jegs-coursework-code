// Define API constants.
const WEB_SERVICE_1_BASE_URL = "https://module-lectures-api.herokuapp.com";
const WEB_SERVICE_2_BASE_URL = "https://comp3211.azurewebsites.net";
const BING_MAPS_BASE_URL = "https://dev.virtualearth.net/REST/v1/Imagery/Map";
const BING_MAPS_API_KEY =
  "Al1r9Vzl9kIf1rOVRPyCOBqXWbTUc9KyeJKxxeyFjfel6sYvPZoq5zBP5GG3Es29";

// Populate module list on page load.
document.addEventListener("DOMContentLoaded", async () => {
  let input = document.getElementById("module-info-input");
  let alert = document.getElementById("module-alert");

  // Disable input and hide previous alerts.
  input.disabled = true;
  alert.hidden = true;

  // Show loading message in input.
  let placeholder = document.createElement("option");
  placeholder.text = "Loading...";
  input.appendChild(placeholder);

  // Get modules from API 1.
  let response = await getJSONFromAPI(`${WEB_SERVICE_1_BASE_URL}/modules`);

  // If valid resopnse, populate list.
  if (response.ok) {
    // Remove loading placeholder.
    input.removeChild(placeholder);

    // For each module, add an option.
    response.json.forEach((element) => {
      let option = document.createElement("option");
      option.text = element["name"];
      option.value = element["id"];
      input.appendChild(option);
    });
    // If error, show alert.
  } else {
    alert.innerText = "There was a server error, please try again later.";
    alert.hidden = false;
  }

  // Enable input.
  input.disabled = false;
});

// Populate module activities table on button press.
document
  .getElementById("module-info-button")
  .addEventListener("click", async () => {
    let input = document.getElementById("module-info-input");
    let moduleID = input.value;
    let table = document.getElementById("module-table").tBodies[0];
    let alert = document.getElementById("module-alert");

    // Disable input and hide previous results.
    input.disabled = true;
    alert.hidden = true;
    table.parentElement.hidden = true;

    // Get module activities from API 1.
    let response = await getJSONFromAPI(
      `${WEB_SERVICE_1_BASE_URL}/lectures/${moduleID}`
    );

    // If valid response, populate table.
    if (response.ok) {
      // Show table and clear body rows.
      table.parentElement.hidden = false;
      table.innerHTML = "";

      // For each activity, add a row to the table.
      response.json.forEach((element) => {
        let tr = document.createElement("tr");

        // Add each attribute to the row except for the database keys.
        Object.keys(element).forEach((attribtue) => {
          if (attribtue != "id" && attribtue != "module_id") {
            let td = document.createElement("td");
            td.innerText = element[attribtue];
            tr.appendChild(td);
          }
        });

        // Append row to table body.
        table.appendChild(tr);
      });
      // If error returned, display appropriate alert.
    } else if (response.status == 404) {
      alert.innerText = "We didn't find a module with that code.";
      alert.hidden = false;
    } else {
      alert.innerText = "There was a server error, please try again later.";
      alert.hidden = false;
    }

    // Enable input.
    input.disabled = false;
  });

// Show building information and map on button press.
document
  .getElementById("building-info-button")
  .addEventListener("click", async () => {
    let input = document.getElementById("building-info-input");
    let roomCode = input.value;
    let table = document.getElementById("building-table").tBodies[0];
    let alert = document.getElementById("building-alert");
    let mapContainer = document.getElementById("map");

    // Disable input and hide previous results and map.
    input.disabled = true;
    alert.hidden = true;
    table.parentElement.hidden = true;
    mapContainer.innerHTML = "";

    // Get building information from API 2.
    let response = await getJSONFromAPI(
      `${WEB_SERVICE_2_BASE_URL}/rooms/${roomCode}/building`
    );

    // If valid response, populate table and show map.
    if (response.ok) {
      // Show table and clear table body.
      table.parentElement.hidden = false;
      table.innerHTML = "";

      let building = response.json;
      let lat = building.coordinates._latitude;
      let lng = building.coordinates._longitude;

      // Helper function to add a row with the given key and value to the table.
      let addRow = (key, value) => {
        // Create row.
        let tr = document.createElement("tr");

        // Create row header and append to row.
        let th = document.createElement("th");
        th.scope = "row";
        th.appendChild(document.createTextNode(key));
        tr.appendChild(th);

        // Create row data cell and append to row.
        let td = document.createElement("td");
        td.appendChild(document.createTextNode(value));
        tr.appendChild(td);

        // Append row to table.
        table.appendChild(tr);
      };

      // Populate table with information from API.
      addRow("Name", building.name);
      addRow("Department", building.department);
      addRow("Latitude", lat);
      addRow("Longitude", lng);

      // Create image element for the map.
      let map = document.createElement("img");
      map.classList.add("img-fluid");

      // Set the image element's source as the Bing Maps API endpoint.
      map.src =
        `${BING_MAPS_BASE_URL}/AerialWithLabels/${lat},${lng}/16?` +
        `mapSize=500,500&pp=${lat},${lng};47&key=${BING_MAPS_API_KEY}`;

      // Append map to container.
      mapContainer.appendChild(map);

      // If response invalid, display appropriate error.
    } else if (response.status == 404) {
      alert.innerText = "We didn't find a room with that code.";
      alert.hidden = false;
    } else {
      alert.innerText = "There was a server error, please try again later.";
      alert.hidden = false;
    }

    // Enable input.
    input.disabled = false;
  });

// Helper function to call an API endpoint and return the response and status.
async function getJSONFromAPI(url) {
  // Fetch API response.
  let response = await fetch(url);

  // If reponse okay return response and status.
  if (response.ok) {
    let json = await response.json();
    return { json: json, ok: response.ok, status: response.status };
    // If error, just return status.
  } else {
    return { ok: response.ok, status: response.status };
  }
}
