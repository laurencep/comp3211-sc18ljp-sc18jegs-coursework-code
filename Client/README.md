# COMP3211 Distributed Systems - Coursework - Client

This repository contains a client written to combine the two web services and third external web service.

It is a static website which can be viewed locally or at https://laurencep.gitlab.io/comp3211-distributed-systems-coursework-client/.
