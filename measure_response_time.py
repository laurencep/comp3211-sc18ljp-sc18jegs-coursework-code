import requests;
import numpy;
import argparse

parser = argparse.ArgumentParser(description='Measure API response time.')
parser.add_argument('url', type=str, help='API endpoint to test.')
parser.add_argument('n', type=int, help='Number of times to run.')

args = parser.parse_args()

results = []

try:
  for i in range(args.n):
    r = requests.get(args.url)
    results.append(r.elapsed.total_seconds())
except Exception as e:
  print(str(e))
  exit()

print(f"Results: {results}")
print(f"Average: {numpy.mean(results)}")
print(f"Standard Deviation: {numpy.std(results)}")